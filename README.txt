1. Master site
-------------

1.1 Download:

- content push
- services
- clients

- At time of writing, you need the patch from http://drupal.org/node/988958#comment-3852528 
for pain-free use of Services. 

1.2 Enable

- clients_drupal
- Services
- xmlrpc
- key authentication
- file service
- node service
- system service
- user service
- content push


1.3. Set up.
1.3.1. Set up Services

- At admin/build/services/settings:
  - Set 'Authentication module' to 'Key authentication';
  Enable:
  - Session IDs for services
  - Use Keys for services

- At admin/build/services/keys:
 Set up individual API keys for each receiving website that you have.
  - Application title: use the name of the receiving site
  - Domain: this need not be an actual domain; you can use the name of the site again.
 Each key needs access to the following methods:
   - system.connect
   - user.logout
   - user.login
 	 - node.get
   - file.get
   - user.get
   (support for comments and taxonomy is not yet available; see Transport module)
   
1.3.2. Set up accounts

Set up a webservice user with a role with access to all permissions below.
It's a good idea to create a specific 'webservice' role for this.
  - load node data
  - get any user data
  - get any binary files
  - (any other permissions according to what content you need to retrieve)

1.3.3. Set up Clients

(The following depend on the receiving site being set up; come back here later)

At admin/settings/clients, add a Drupal Services 6.x-2.x connection for each receiving site
  - name: whatever you like; something that identifies the receiving site.
  - the following should be self-evident
  - you can ignore 'services' and 'resources' boxes. 

At this point you can go to the connection test page. You should be able to 
connect and log in.

1.3.4. Set up Content push

At admin/settings/content_push, enter the same endpoint you give for the 
client connection to the master site for each receiving site in 2.3.3.

1.3.5. Set up Content types

You should ensure you have matching content types on both sites.
A simple way of doing this is with Features module.

Enable each content type you want to be distributable at the content type admin page. 


2. Receiving site
-----------------

On each receiving site:

2.1. Download:

- services
- clients
- transport
- autoload
- faces

2.2. Enable:

  - Services
  - key authentication
  - XMLRPC server
  - System Service
  - User Service
  - Transport service
  - Clients Base
  - Drupal Services
  - Transport

2.3. Set up.
2.3.1. Set up Services

- At admin/build/services/settings:
  - Set 'Authentication module' to 'Key authentication';
  Enable:
  - Session IDs for services
  - Use Keys for services

- At admin/build/services/keys:
 Set up an API key for the master site.
  - Application title: use the name of the master site
  - Domain: this need not be an actual domain; you can use the name of the site again.
 Each key needs access to the following methods:
   - system.connect
   - user.login
   - user.logout
   - transport.retrieve

2.3.2. Set up accounts

Set up a webservice user with a role with access to all permissions below.
It's a good idea to create a specific 'webservice' role for this.
 - access tranport service   
   
2.3.3. Set up Clients

At admin/settings/clients, add a Drupal Services 6.x-2.x connection.
  - name: whatever you like; 'master' is a good choice
  - the following should be self-evident
  - you can ignore 'services' and 'resources' boxes. 

At this point you can go to the connection test page. You should be able to 
connect, log in, and do a test load of a remote node from the master site.
   
(You can go back and do 1.3.3 onwards now.)   

2.3.4 Popular gotchas

  - if nothing has previously been saved in a particular filefield, it will fail to save
  - refresh the cache so autoload spots all the class files.
   
   
   
