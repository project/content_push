<?php
// $Id$
/**
 * @file content_push.module
 * Contains admin callbacks for the Content push module.
 */

/**
 * Form builder for the transport demo settings.
 */
function content_push_admin_form($form_state) {
  $form['content_push_local_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Local endpoint URL'),
    '#description' => t("The URL of this site's XMLRPC endpoint. This should match the endpoint set for Client connections on receiving sites."),
    '#default_value' => variable_get('content_push_local_endpoint', ''),
  );

  return system_settings_form($form);
}
